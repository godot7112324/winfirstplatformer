extends Node2D
#Green|Slime
const SPEED: int = 60;
var direction = 1;

@onready var ray_cast_2d_r = $RayCast2D_R
@onready var ray_cast_2d_l = $RayCast2D_L


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if ray_cast_2d_r.is_colliding():
		direction = 1;
	elif ray_cast_2d_l.is_colliding():
		direction = -1;
	position.x += direction*SPEED*delta;
	#pass
